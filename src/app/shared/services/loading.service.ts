import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  public showLoad = new BehaviorSubject<boolean>(false);

  showTitleObservable = this.showLoad.asObservable();


  showLoadPage(value: boolean): void {
    this.showLoad.next(value);
  }


}
