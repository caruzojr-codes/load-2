import { Component, OnInit } from '@angular/core';

import { LoadingService } from './../../services/loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  public hasShowLoad: boolean;

  constructor(
    private loadService: LoadingService
  ) { }

  ngOnInit(): void {

    this.loadService.showTitleObservable.subscribe((result) => {
      this.hasShowLoad = result;
    });

  }

}
