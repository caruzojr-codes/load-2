import { Component, OnInit } from '@angular/core';

import { LoadingService } from './../../shared/services/loading.service';
import { UsuariosService } from './../../shared/services/usuarios.service'

import { Usuario } from './../../shared/models/usuario.model';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  constructor(
    private loadingService: LoadingService,
    private usuariosService: UsuariosService
  ) { }

  ngOnInit() {
    this.loadingService.showLoadPage(true);
    this.getUsuarios();
  }

  getUsuarios() {
    return this.usuariosService.getUsuarios()
      .subscribe((result: Usuario[]) => {
        console.log('Agora vc está em portfolio');
        console.log(result);
        this.loadingService.showLoadPage(false);
      },
      error => console.log(error))
  }

}
