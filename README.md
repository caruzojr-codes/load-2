# Load
Como criar um simples load em tela enquanto a leitura da API seja feita.

Diferente das versão 1 do load, essa versão utiliza a passagens de dados via serviço, uma alternativa diferente, mais simples e fácil de trabalhar.



## Run Project
Clone este repositório para a máquina local
```
git clone https://caruzojr@bitbucket.org/caruzojr/load.git
```
Dentro da pasta do projeto, execute o comando abaixo para baixar todas as dependencias necessárias
```
npm install
```
Rode o seguinte comando para executar o projeto
```
ng s --open
```
Para rodar o mock do json-serve, abra um novo terminal, e dentro da pasta `dbmock`, rode o seguinte comando
```
json-server --watch db.json
```
